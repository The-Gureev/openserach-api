import fetch from 'node-fetch';
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const number = 1000000;

let i = 1;

const urlConnect = 'https://admin:admin@localhost:9200/mans';
const headersList = {
    'Accept': '*/*',
    'Content-Type': 'application/json'
};

while (i <= number) {
    i++;
    let date1 = new Date();
    let date2 = new Date();
    date1.setDate(date1.getDate()-Math.ceil((1000+Math.random()*30000)));
    date2.setDate(date1.getDate()-Math.ceil(Math.random()*1000));

    const timeDiff = Math.abs(date2.getTime() - date1.getTime());
    const age = Math.ceil(timeDiff / (1000 * 3600 * 24 * 365)); 
    
    const person = {
        name: generateWord(),
        patronymic: generateWord(),
        surname: generateWord(),
        age,
        d1: `${datePrep(date1.getDate())}.${datePrep(date1.getMonth()+1)}.${date1.getFullYear()}`,
        d2: `${datePrep(date2.getDate())}.${datePrep(date2.getMonth()+1)}.${date2.getFullYear()}`
    }

    const response = await fetch(`${urlConnect}/_doc`, {
        method: 'POST',
        headers: headersList,
        body: JSON.stringify(person)
    });
    
}

function generateQuery() {

}

function datePrep(date) {
    return date<10 ? '0'+date : date
}

function generateWord() {
    const letters = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
    let word = '';
    while(word.length<=10) {
        word += letters[Math.ceil(Math.random()*32)];
    }
    return word;
}
