# OpenSearch Api


## Команды

Примеры: https://techexpert.tips/ru/elasticsearch-ru/elasticsearch-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BD%D0%B4%D0%B5%D0%BA%D1%81%D0%B0/

Создание индекса mans
```
PUT  https://admin:admin@localhost:9200/mans
```

Добавление полей в индекс
```
PUT  https://admin:admin@localhost:9200/mans/_mapping?pretty
{
  "properties": {
    "name":  { "type": "text"},
    "surname":  { "type": "text"},
    "patronymic":  { "type": "text"},
    "age":  { "type": "integer"}
  }
}
```

Добавление данных в индекс
```
POST "http://admin:admin@localhost:9200/mans/_doc/?pretty" -H 'Content-Type: application/json' -d'
{   
    "name" : "Сергей",
	  "surname" : "Дьяконов",
    "patronymic" : "Сергеевич",
    "age" : 99
}'
```

curl  -X GET "http://192.168.100.7:9200/accounts/_search?pretty=true&q=*:*"


Поиск в индексе
```
GET https://admin:admin@localhost:9200/mans/_search?pretty
{
  "query": {
    "bool": {
      "should": [
        {
          "match_bool_prefix": {
            "surname": "ывв"
          }
        },
        {
          "match_bool_prefix": {
            "name": "серг"
          }
        },
        {
          "match_bool_prefix": {
            "d1": "01.01.1990"
          }
        }
      ]
    }
  }
}
```