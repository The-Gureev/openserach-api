import config from 'config'
import db from 'mysql2/promise'

const login = config.get('PostgreSQL.login');
const password = config.get('PostgreSQL.password');
const prot = config.get('PostgreSQL.prot');
const host = config.get('PostgreSQL.host');
const database = config.get('PostgreSQL.database');
let connection = null;

export const connect = async () => {
    connection = await db.createConnection({
        host,
        user: login,
        database,
        password
    });
}

export const find = async (fields, table, primaryKey, index, ordering = '') => {
    const idx = index.join(',');
    const sql = `SELECT ${fields} FROM ${table} WHERE ${primaryKey} IN (${idx}) ${ordering}`;
    const [res] = await connection.query(sql);
    return res;
}


export const disconnect = async () => {
    // закрытие подключения
    connection.end(function (err) {
        if (err) {
            return console.log('Ошибка: ' + err.message);
        }
        console.log('Подключение закрыто');
    });
}


export class DataBase {
    constructor(configConnect = {}) {
        const {
            login,
            password,
            prot,
            host,
            database
        } = configConnect;
        this.login = login;
        this.password = password;
        this.prot = prot;
        this.host = host;
        this.database = database;
    }

    // Подключение
    connect = async () => {
        this.connection = await db.createConnection({
            host: this.host,
            user: this.login,
            database: this.database,
            password: this.password
        });
    }

    // Поиск
    find = async (fields, table, primaryKey, index, ordering = '') => {
        const idx = index.join(',');
        const sql = `SELECT ${fields} FROM ${table} WHERE ${primaryKey} IN (${idx}) ${ordering}`;
        const [res] = await this.connection.query(sql);
        return res;
    }

    disconnect = async () => {
        // закрытие подключения
        this.connection.end(function (err) {
            if (err) {
                return console.log('Ошибка: ' + err.message);
            }
            console.log('Подключение закрыто');
        });
    }
}