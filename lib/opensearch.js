//import config from 'config';
import fetch from 'node-fetch';
/*
const login = config.get('OpenSearch.login');
const password = config.get('OpenSearch.password');
const prot = config.get('OpenSearch.prot');
const port = config.get('OpenSearch.port');
const host = config.get('OpenSearch.host');

const urlConnect = `${prot}${login}:${password}@${host}:${port}/`;
*/
// Добавление документа и обновление
/*
Пример данных для отправки
{
  "key": "name",
  "value": "Мария",
  "date": "01-03-2023"
}
*/
/*
export const update = async (index, id, data) => {
    try {
        // const urlConnect = `${prot}${login}:${password}@${host}/opensearch/`;
        const response = await fetch(`${urlConnect}${index}/_doc/${id}`, {
            method: 'PUT',
            headers: headersList,
            body: JSON.stringify(data)
        });
        return response;
    } catch (e) {
        console.log('Ошибка ', e);
    }
}*/

// Поиск
/*
{
  "query": {
    "match_all": {}
  }
}
*/
/*
export const search = async (index, data) => {
    try {
        const response = await fetch(`${urlConnect}${index}/_search?pretty`, {
            method: 'POST',
            headers: headersList,
            body: JSON.stringify(data)
        });
        return response;
    } catch (e) {
        console.log('Ошибка ', e);
    }
}*/


// Добавление индекса
/*
Пример данных для отправки
{
    "settings" : {
        "number_of_shards" : 2,
        "number_of_replicas" : 1
    },
    "mappings" : {
        "properties" : {
            "tags" : { "type" : "value" },
            "updated_at" : { "type" : "date" }
        }
    }
}
*/
/*
export const createIndex = async (index, id, data) => {
    try {
        const response = await fetch(`${urlConnect}${index}`, {
            method: 'PUT',
            headers: headersList,
            body: JSON.stringify(data)
        });
        return response;
    } catch (e) {
        console.log('Ошибка ', e);
    }
}*/

export class OpenSearch {
    
    constructor (configConnect) {
        if(configConnect) {
            this.setConfig(configConnect)
        }
    }

    setConfig(configConnect = {}) {
        const {
            login,
            password,
            prot,
            host,
            port
        } = configConnect;

        this.login = login;
        this.password = password;
        this.prot = prot;
        this.host = host;
        this.port = port;

        this.headersList = {
            'Accept': '*/*',
            'Content-Type': 'application/json'
        };

        this.urlConnect = `${prot}${login}:${password}@${host}:${port}/`;
    }

    // Добавление индекса
    async createIndex(index, id, data) {
        try {
            
            const response = await fetch(`${this.urlConnect}${index}`, {
                method: 'PUT',
                headers: this.headersList,
                body: JSON.stringify(data)
            });
            return response;
        } catch (e) {
            console.log('Ошибка ', e);
        }
    }

    search = async (index, data) => {
        try {
            const query = {
                "query": data
            };

            const response = await fetch(`${this.urlConnect}${index}/_search?pretty`, {
                method: 'POST',
                headers: this.headersList,
                body: JSON.stringify(query)
            });

            return await response.json();
        } catch (e) {
            console.log('Ошибка ', e);
        }
    }

    async update (index, id, data) {
        try {
            const response = await fetch(`${this.urlConnect}${index}/_doc/${id}`, {
                method: 'PUT',
                headers: this.headersList,
                body: JSON.stringify(data)
            });
            return response;
        } catch (e) {
            console.log('Ошибка ', e);
        }
    }
}