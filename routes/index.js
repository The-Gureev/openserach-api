import Search from './search/search.route.js'

export default ({ app }) => {
    app.use('/search', Search)
}
