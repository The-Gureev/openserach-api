import {
    Router
} from 'express';
import SearchController from './search.controller.js';


class SearchRoute {
    router = Router();
    searchController = new SearchController();

    constructor() {
        this.initializeRoutes();

    }

    initializeRoutes() {
        this.router.get('/', this.searchController.get);
        this.router.get('/echo', this.searchController.echo);
        this.router.post('/any', this.searchController.any);
    }

}

export default new SearchRoute().router