import {
    find
} from '../../lib/database.js';
import {
    OpenSearch
} from '../../lib/opensearch.js';
import config from 'config';


class SearchController {
    openSearch = new OpenSearch();

    constructor() {
        const login = config.get('OpenSearch.login');
        const password = config.get('OpenSearch.password');
        const prot = config.get('OpenSearch.prot');
        const port = config.get('OpenSearch.port');
        const host = config.get('OpenSearch.host');

        this.openSearch.setConfig({
            login,
            password,
            prot,
            port,
            host
        });
    }

    any = async (req, res) => {
        const search = req.body.search; // Название search в OpenSearch
        const index = req.body.index; // Название search в OpenSearch
        console.log(search, index)
        if (!search) return res.status(200).json({
            status: 200,
            message: 'Не валидные данные для поиска'
        });

        // Делаем поиск в OpenSearch
        const response = await this.openSearch.search(index, search);
        
        res.status(200).json({
            status: 200,
            data: response
        });
    }

    get = async (req, res) => {
        const key = req.query.key; // Название ключа в OpenSearch
        const searchValue = req.query.search; // Строка по которой выполняется поиск
        const index = req.query.index; // Индекс по которому делаем поиск


        if (!key || !index) return res.status(200).json({
            status: 200,
            message: 'Не валидные данные для поиска'
        });

        // Поиск
        // Примеры поиска https://habr.com/ru/post/280488/
        const searchObject = {
            "query": {
                "match": {
                    [key]: searchValue
                }
            }
        };

        // Делаем поиск в OpenSearch
        const response = await this.openSearch.search(index, searchObject);
        const data = await response.json();

        const indexsesOpenSearch = data.hits.hits.map(el => el._id);

        // Поиск по реляционной базе
        //const dataBaseSearch = await find('*', 'oc_extension', 'extension_id', indexsesOpenSearch);
        
        res.status(200).json({
            status: 200,
            data,
            indexsesOpenSearch,
            //data: "dataBaseSearch",
            search: "req.query.search"
        });
    }

    echo = (req, res) => {
        res.status(200).json({
            test: "test"
        });
    }

}

export default SearchController