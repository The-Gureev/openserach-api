import {
    connect,
    find
} from './lib/database.js';

import express from "express";
import config from "config";
import cors from "cors";
import routes from "./routes/index.js";
import bodyParser from "body-parser";

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;


const app = express();
app.use(cors());
app.use(express.json());

const PORT = config.get('Server.Port');

const start = async function () {
    // await connect();
    app.listen(3000, () => {
        return console.log(`server is listening on ${PORT}`);
    });
    routes({
        app
    });

}

// Запуск сервера
start();